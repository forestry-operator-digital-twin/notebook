import torch
from pathlib import Path
import json
from torch.utils.data import Dataset
from sklearn.preprocessing import MinMaxScaler
import numpy as np


# ref https://pytorch.org/tutorials/beginner/basics/data_tutorial.html

# https://pytorch.org/docs/stable/generated/torch.clamp.html
class ObservationStateDataset(Dataset):
    def __init__(self,path,n_state):
        self.raw_data = self._load_json(path)
        self._flatten_data_set()
        self.n_state = n_state
        self._arrange_data_into_x_y()

    def __len__(self):
        return len(self.raw_data)

    def __getitem__(self, idx):
        return torch.FloatTensor(self.X[idx]), torch.FloatTensor(self.y[idx])
    
    def _flatten_data_set(self):
        dataset = []
        for _,v in self.raw_data.items():
            dataset += v
        self.raw_data = dataset
        
    def _arrange_data_into_x_y(self):
        X = []
        y = []
        for el in self.raw_data:
            X.append(el["observation"])
            
            current_y = [0] *(self.n_state)
            current_y[el["state"]] = 1
            y.append(current_y)
        self.X = X
        self.y = y    
    
    def _load_json(self,path):
        with open(path,'r') as f:
            data = json.load(f)["data"]
            return data
    def to_x_y(self):
        X = np.array(self.X)
        y = [el.index(1) for el in self.y]
        return X, np.array(y)
        
        
class ObservationDatasetSequence(Dataset):
    def __init__(self,path):
        self.raw_data = list(self._load_json(path).values())
        self._arrange_data_into_x_y()

    def __len__(self):
        return len(self.raw_data)

    def __getitem__(self, idx):        
        
        return torch.FloatTensor(self.X[idx]), torch.FloatTensor(self.y[idx]), torch.FloatTensor(self.y[idx])
    
    def _arrange_data_into_x_y(self):
        X = []
        y = []
        for el in self.raw_data:
            currentX = []
            currentY = []
            for i in range(0, len(el)-1):
                currentX.append(el[i])
                currentY.append(el[i+1])
            X.append(currentX)
            y.append(currentY)
        self.X = X
        self.y = y    
    
    def _load_json(self,path):
        with open(path,'r') as f:
            data = json.load(f)["data"]
            return data

class ObservationStateSequenceDataset(Dataset):
    def __init__(self,path,n_state, slice_length = None):
        self.raw_data = list(self._load_json(path).values())
        self.n_state = n_state
        self.slice_length = slice_length
        if slice_length != None:
            self._slice_dataset()
        else:
            self._arrange_data_into_x_y_z()
        

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        return torch.FloatTensor(self.X[idx]), torch.FloatTensor(self.y[idx]), torch.FloatTensor(self.z[idx])
    
    def _flatten_data_set(self):
        dataset = []
        for _,v in self.raw_data.items():
            dataset += v
        self.raw_data = dataset
        
    def _slice_dataset(self):
        X = []
        y = []
        z = []
        for el in self.raw_data:
            currentX = []
            currentY = []
            currentZ = []
            for i, nested_el in enumerate(el):
                currentX.append(nested_el["observation"])
                next_label = [0] *(self.n_state)
                next_label[nested_el["next_state"]] = 1
                currentY.append(next_label)
                currentZ.append(nested_el["next_observation"])
                
                if (i+1)%self.slice_length == 0:
                    X.append(currentX)
                    y.append(currentY)
                    z.append(currentZ)
                    currentX = []
                    currentY = []
                    currentZ = []
           
        self.X = X
        self.y = y  
        self.z = z
        
    def _arrange_data_into_x_y_z(self):
        X = []
        y = []
        z = []
        for el in self.raw_data:
            currentX = []
            currentY = []
            currentZ = []
            for nested_el in el:
                currentX.append(nested_el["observation"])
                next_label = [0] *(self.n_state)
                next_label[nested_el["next_state"]] = 1
                currentY.append(next_label)
                currentZ.append(nested_el["next_observation"])
                
            X.append(currentX)
            y.append(currentY)
            z.append(currentZ)
           
        self.X = X
        self.y = y  
        self.z = z
    
    def _load_json(self,path):
        with open(path,'r') as f:
            data = json.load(f)["data"]
            return data

class ObservationStateSliceDataset(Dataset):
    def __init__(self,path,n_state, slice_length):
        self.raw_data = list(self._load_json(path).values())
        self.n_state = n_state
        self.slice_length = slice_length
        self._arrange_data_into_x_y()
        self._divide_into_slice()

    def __len__(self):
        return len(self.y)

    def __getitem__(self, idx):
        return torch.transpose(torch.FloatTensor(self.X[idx]),0,1) , torch.FloatTensor(self.y[idx])
    
    
    def _flatten_data_set(self):
        dataset = []
        for _,v in self.raw_data.items():
            dataset += v
        self.raw_data = dataset
        
    def _arrange_data_into_x_y(self):
        X = []
        y = []
        for el in self.raw_data:
            currentX = []
            currentY = []
            for nested_el in el:
                currentX.append(nested_el["observation"])
                next_label = [0] *(self.n_state)
                next_label[nested_el["state"]] = 1
                currentY.append(next_label)
                
            X.append(currentX)
            y.append(currentY)
           
        self.X = X
        self.y = y
        
    def _divide_into_slice(self):
        X = []
        y = []
        for i, series in enumerate(self.X):
            currentX = []
            for j, x in enumerate(series):
                currentX.append(x)
                if (j+1)%self.slice_length == 0:
                    X.append(currentX)
                    currentX = []
                    y.append(self.y[i][j])
        self.X = X
        self.y = y
    
    def _load_json(self,path):
        with open(path,'r') as f:
            data = json.load(f)["data"]
            return data

        
        