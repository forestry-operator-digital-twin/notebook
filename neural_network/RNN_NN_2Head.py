import torch
from torch import nn
import torch.nn.functional as F
from collections import OrderedDict

from .hyperParameter import NnParam, RnnParam,RnnType


class TwoHeadNetwork(nn.Module):

    def __init__(self, rnn_param, nn_param):
        super(TwoHeadNetwork, self).__init__()
        self.rnn_param = rnn_param
        self.nn_param = nn_param

        if (rnn_param.prediction_network == RnnType.GRU) or (rnn_param.prediction_network == str(RnnType.GRU)):
            self.create_gru_base()
        elif (rnn_param.prediction_network == RnnType.LSTM) or (rnn_param.prediction_network == str(RnnType.LSTM)):
            self.create_lstm_base()
        else:
            raise ValueError('not a valid prediction network defined') 

        self.create_nn_head_classification()
        self.create_nn_head_prediction()

    def create_nn_head_classification(self):

        n_hidden_feature = self.rnn_param.n_hidden_feature
        n_class = self.nn_param.n_class
        prob_dropout = self.nn_param.prob_dropout

        self.layers_classification_head = OrderedDict()

        for i in range(self.nn_param.n_depth):
            self.layers_classification_head["hidden_layer{}".format(
                i)] = nn.Linear(n_hidden_feature, n_hidden_feature)

        self.layers_classification_head["output_layer"] = nn.Linear(
            n_hidden_feature, n_class)
        self.layers_classification_head["dropout_layer"] = nn.Dropout(
            p=prob_dropout)
        self.nn_classification_head = nn.Sequential(
            self.layers_classification_head)

    def create_nn_head_prediction(self):
        n_hidden_feature = self.rnn_param.n_hidden_feature
        n_feature = self.rnn_param.n_feature

        self.layers_prediction_head = OrderedDict()

        for i in range(self.rnn_param.n_depth):
            self.layers_prediction_head["hidden_layer_{}".format(
                i)] = nn.Linear(n_hidden_feature, n_hidden_feature)

        self.layers_prediction_head["output_layer"] = nn.Linear(
            n_hidden_feature, n_feature)
        self.nn_prediction_head = nn.Sequential(self.layers_prediction_head)

    # https://blog.floydhub.com/gru-with-pytorch/
    # https://pytorch.org/docs/stable/generated/torch.nn.GRU.html

    def create_gru_base(self):
        self.predictor = nn.GRU(input_size=self.rnn_param.n_feature,
                                hidden_size=self.rnn_param.n_hidden_feature,
                                num_layers=self.rnn_param.n_prediction_layers, batch_first=True,
                                dropout=self.rnn_param.prediction_dropout_prob)

    # https://pytorch.org/docs/stable/generated/torch.nn.LSTM.html?highlight=lstm#torch.nn.LSTM
    def create_lstm_base(self):
        self.predictor = nn.LSTM(input_size=self.rnn_param.n_feature,
                                 hidden_size=self.rnn_param.n_hidden_feature,
                                 num_layers=self.rnn_param.n_prediction_layers, batch_first=True,
                                 dropout=self.rnn_param.prediction_dropout_prob)

    # https://blog.floydhub.com/gru-with-pytorch/
    def init_hidden(self, batch_size, device=None):
        weight = next(self.parameters()).data
        hidden = weight.new(self.rnn_param.n_prediction_layers, batch_size,
                            self.rnn_param.n_hidden_feature).zero_().to(device)
        if self.rnn_param.prediction_network == RnnType.GRU:
            return hidden
        elif self.rnn_param.prediction_network == RnnType.LSTM:
            return (hidden, hidden)

    def forward(self, x, h):
        x, h = self.forward_predictor(x, h)
        x1 = self.forward_prediction_head(x)
        x2 = self.forward_classification_head(x)

        return (x1, x2), h

    # https://blog.floydhub.com/gru-with-pytorch/
    # https://pytorch.org/docs/stable/generated/torch.nn.LSTM.html?highlight=lstm#torch.nn.LSTM
    # https://pytorch.org/docs/stable/generated/torch.nn.GRU.html
    def forward_predictor(self, x, h):
        x, h = self.predictor(x, h)
        x = x[0].detach()
        return x, h

    def forward_classification_head(self, x):
        for layer in self.nn_classification_head:
            x = self.nn_param.activation_function(layer(x))

        return x

    def forward_prediction_head(self, x):
        for layer in self.nn_prediction_head:
            x = self.rnn_param.activation_function(layer(x))
        return x
