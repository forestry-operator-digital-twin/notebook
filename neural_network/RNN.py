import torch
from torch import nn
import torch.nn.functional as F
from collections import OrderedDict

from .hyperParameter import RnnType
# https://pytorch.org/tutorials/beginner/blitz/neural_networks_tutorial.html


class RNN(nn.Module):

    def __init__(self, n_feature, n_hidden_feature, n_output_feature, n_dept, prediction_network, n_prediction_layers, activation_function, prediction_dropout_prob, with_batch_norm=False):
        super(RNN, self).__init__()
        self.n_feature = n_feature
        self.n_hidden_feature = n_hidden_feature
        self.activation_function = activation_function
        self.n_prediction_layers = n_prediction_layers
        self.prediction_dropout_prob = 0 if n_prediction_layers ==1 else prediction_dropout_prob
        self.prediction_network = prediction_network
        self.n_dept = n_dept
        self.n_output_feature = n_output_feature
        self.with_batch_norm = with_batch_norm

        if (prediction_network == RnnType.GRU) or (prediction_network == str(RnnType.GRU)):
            self.create_gru_base()
        elif (prediction_network == RnnType.LSTM) or (prediction_network == str(RnnType.LSTM)):
            self.create_lstm_base()
        else:
            raise ValueError('not a valid prediction network defined') 

        self.create_output_nn()

    # https://blog.floydhub.com/gru-with-pytorch/
    # https://pytorch.org/docs/stable/generated/torch.nn.GRU.html
    def create_gru_base(self):
        self.predictor = nn.GRU(input_size=self.n_feature,
                                hidden_size=self.n_hidden_feature,
                                num_layers=self.n_prediction_layers, batch_first=True,
                                dropout=self.prediction_dropout_prob)

    # https://pytorch.org/docs/stable/generated/torch.nn.LSTM.html?highlight=lstm#torch.nn.LSTM
    def create_lstm_base(self):
        self.predictor = nn.LSTM(input_size=self.n_feature,
                                 hidden_size=self.n_hidden_feature,
                                 num_layers=self.n_prediction_layers, batch_first=True,
                                 dropout=self.prediction_dropout_prob)

    def create_output_nn(self):
        self.layer_output_nn = OrderedDict()
        if self.with_batch_norm:
            self.layer_output_nn["batch_norm"] = nn.BatchNorm1d(self.n_hidden_feature)

        for i in range(self.n_dept):
            self.layer_output_nn["hidden_layer_{}".format(i)] = nn.Linear(
                self.n_hidden_feature, self.n_hidden_feature)

        self.layer_output_nn["output_layer"] = nn.Linear(
            self.n_hidden_feature, self.n_output_feature)
        self.output_nn = nn.Sequential(self.layer_output_nn)

    # https://blog.floydhub.com/gru-with-pytorch/
    def init_hidden(self, batch_size, device=None):
        weight = next(self.parameters()).data
        hidden = weight.new(self.n_prediction_layers, batch_size,
                            self.n_hidden_feature).zero_().to(device)
        if self.prediction_network == RnnType.GRU:
            return hidden
        elif self.prediction_network == RnnType.LSTM:
            return (hidden, hidden)

    '''
    pass the sequence into the RnnType
    convert to sequnce into a pad sequence
    get the last element of every sequence of every batch
    '''
    # https://discuss.pytorch.org/t/inplace-operation-error-using-lstm-gru/87190

    def forward(self, x, h):
        x, h = self.predictor(x, h)
        x = x[0].detach()

        for layer in self.output_nn:
            x = self.activation_function(layer(x))

        return x, h