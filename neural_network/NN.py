import torch
from torch import nn
import torch.nn.functional as F
from collections import OrderedDict


# https://pytorch.org/tutorials/beginner/blitz/neural_networks_tutorial.html

class NN(nn.Module):

    def __init__(self, n_feature, n_class, n_hidden_feature, n_depth, activation_function, prob_dropout, with_batch_norm):
        super(NN, self).__init__()
        self.n_feature = n_feature
        self.n_class = n_class
        self.n_hidden_feature = n_hidden_feature
        self.activation_function = activation_function
        self.prob_dropout = prob_dropout
        self.n_depth = n_depth
        self.with_batch_norm = with_batch_norm

        self.create_layers()

    def create_layers(self):
        self.layers = OrderedDict()
        if self.with_batch_norm:
            self.layers["batch_norm"] = nn.BatchNorm1d(self.n_feature)
        self.layers["input_layer"] = nn.Linear(self.n_feature, self.n_hidden_feature)

        for i in range(self.n_depth-1):
            self.layers["hidden_layer{}".format(i)] = nn.Linear(
                self.n_hidden_feature, self.n_hidden_feature)

        self.layers["output_layer"] = nn.Linear(
            self.n_hidden_feature, self.n_class)
        self.layers["dropout_layer"] = nn.Dropout(p=self.prob_dropout)
        
        self.layers = nn.Sequential(self.layers)

    def forward(self, x):
        for layer in self.layers:
            x = self.activation_function(layer(x))
        return x
