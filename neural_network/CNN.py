import torch
from torch import nn
import torch.nn.functional as F
from collections import OrderedDict

# https://stackoverflow.com/questions/59125208/how-to-properly-implement-1d-cnn-for-numerical-data-in-pytorch
class CNN(nn.Module):

    def __init__(self, n_feature, n_class, n_hidden_feature, n_convolution_channel, kernel_size, slice_length, n_depth, activation_function, prob_dropout, with_batch_norm):
        super(CNN, self).__init__()
        self.n_feature = n_feature
        self.n_class = n_class
        self.n_hidden_feature = n_hidden_feature
        self.activation_function = activation_function
        self.prob_dropout = prob_dropout
        self.n_depth = n_depth
        self.n_convolution_channel = n_convolution_channel
        self.kernel_size = kernel_size
        self.with_batch_norm = with_batch_norm
        self.slice_length = slice_length
        self.create_layers()

    def create_layers(self):
        self.convolution_layers = OrderedDict()
        self.nn_layers = OrderedDict()
        if self.with_batch_norm:
            self.convolution_layers["batch_norm"] = nn.BatchNorm1d(self.n_feature)
        ## convolution
        self.convolution_layers["convolution_input"] = nn.Conv1d(in_channels = self.n_feature,
                                               out_channels = self.n_convolution_channel,
                                               kernel_size = self.kernel_size)
        padding = 0
        dilatation = 1
        stride = 1
        lout = int(((self.slice_length+2*padding-dilatation*(self.kernel_size-1)-1)/stride)+1)
        self.convolution_layers["maxPool"] = nn.MaxPool1d(kernel_size = lout)

        ## nn        
        self.nn_layers["input_layer_nn"] =  nn.Linear( self.n_convolution_channel, self.n_hidden_feature)

        for i in range(self.n_depth-1):
            self.nn_layers["hidden_layer_{}".format(i)] = nn.Linear(
                self.n_hidden_feature, self.n_hidden_feature)

        self.nn_layers["output_layer"] = nn.Linear(
            self.n_hidden_feature, self.n_class)
        self.nn_layers["dropout_layer"] = nn.Dropout(p=self.prob_dropout)
        self.nn_layers["softmax"] = nn.Softmax(dim=1)
        
        self.convolution_layers = nn.Sequential(self.convolution_layers)
        self.nn_layers = nn.Sequential(self.nn_layers)

    def forward(self, x):
        for layer in self.convolution_layers:
            x = self.activation_function(layer(x))
            
        x = x[:,:,0]
        
        for layer in self.nn_layers:
            x = self.activation_function(layer(x))
        return x