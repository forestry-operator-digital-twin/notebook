from enum import Enum, auto
from torch import nn
import torch.optim as optim

class NnParam():
    def __init__(self, n_class, n_depth, activation_function, prob_dropout):
        self.n_class = n_class
        self.activation_function = activation_function
        self.n_depth = n_depth
        self.prob_dropout = prob_dropout


class RnnParam():
    def __init__(self, n_feature, n_hidden_feature, n_depth, prediction_network, n_prediction_layers, activation_function, prediction_dropout_prob):
        self.n_feature = n_feature
        self.n_hidden_feature = n_hidden_feature
        self.activation_function = activation_function
        self.n_prediction_layers = n_prediction_layers
        self.prediction_dropout_prob = prediction_dropout_prob if n_prediction_layers != 0 else 0
        self.prediction_network = prediction_network
        self.n_depth = n_depth

class AutoName(Enum):
    def _generate_next_value_(name, start, count, last_values):
        return name


class Loss(AutoName):
    CrossEntropyLoss = auto()
    MSELoss = auto()
    L1Loss = auto()

class Optimiser(AutoName):
    Adam = auto()
    AdamDelta = auto()
    AdamAsm = auto()
    SGD = auto()

class ActivationFunction(AutoName):
    Tanh = auto()
    ReLU = auto()
    Softmax = auto()
    Sigmoid = auto()


def loss_factory(name):
    if name == str(Loss.CrossEntropyLoss):
        return nn.CrossEntropyLoss()
    elif name == str(Loss.MSELoss):
        return nn.MSELoss()
    elif name == str(Loss.L1Loss):
        return nn.L1Loss()


def activation_function_factory(name):
    if name == str(ActivationFunction.Tanh):
        return nn.Tanh()
    elif name == str(ActivationFunction.ReLU):
        return nn.ReLU()
    elif name == str(ActivationFunction.Softmax):
        return nn.Softmax(dim=1)
    elif name == str(ActivationFunction.Sigmoid):
        return nn.Sigmoid()
    else:
        raise ValueError("activation function is not valid with {}".format(name))


class RnnType(AutoName):
    GRU = auto()
    LSTM = auto()

def optimiser_factory(name, lr, net):
    if name == str(Optimiser.Adam):
        return optim.Adam(net.parameters(), lr=lr)
    elif name == str(Optimiser.AdamDelta):
        return optim.Adadelta(net.parameters(), lr=lr)
    elif name == str(Optimiser.SGD):
        return optim.SGD(net.parameters(), lr=lr, momentum=0.9)
    elif name == str(Optimiser.AdamAsm):
        return optim.Adam(net.parameters(), lr=lr, amsgrad=True)
    else:
        raise ValueError("optimiser is not valid with {}".format(str(name)))