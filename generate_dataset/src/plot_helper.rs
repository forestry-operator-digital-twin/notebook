use plotters;
use plotters::prelude::*;

pub(crate) fn histogram(
    input_data: &Vec<usize>,
    n_observation: usize,
    max_occurence: u32,
    x_label: String,
    margin: u32,
    filename: &String,
) {
    let root = BitMapBackend::new(filename, (1024, 768)).into_drawing_area();
    root.fill(&WHITE).unwrap();

    let mut chart = ChartBuilder::on(&root)
        .x_label_area_size(35u32)
        .y_label_area_size(50u32)
        .margin(5u32)
        .build_cartesian_2d(
            (0u32..n_observation as u32).into_segmented(),
            0u32..max_occurence,
        )
        .unwrap();

    chart
        .configure_mesh()
        .bold_line_style(&WHITE.mix(0.3))
        .y_desc("number of occurrences")
        .x_desc(x_label)
        .axis_desc_style(("sans-serif", 15f32))
        .draw()
        .unwrap();
    chart
        .draw_series(
            Histogram::vertical(&chart)
                .style(GREEN.filled())
                .margin(margin)
                .data(input_data.iter().map(|x: &usize| (*x as u32, 1))),
        )
        .unwrap();
}

pub(crate) fn serie_float(input_data: &Vec<f32>, highest_value: f32, filename: &String) {
    let root = BitMapBackend::new(filename, (1024, 768)).into_drawing_area();
    root.fill(&WHITE).unwrap();

    let mut chart = ChartBuilder::on(&root)
        .margin(10)
        .x_label_area_size(30u32)
        .y_label_area_size(60u32)
        .build_cartesian_2d(0f32..input_data.len() as f32 + 2.0, 0.0..highest_value)
        .unwrap();

    chart
        .configure_mesh()
        .bold_line_style(&WHITE.mix(0.2))
        .y_desc("Observation")
        .x_desc("step")
        .draw()
        .unwrap();

    chart
        .draw_series(LineSeries::new(
            (0..input_data.len())
                .map(|x| x as usize)
                .map(|x| (x as f32, input_data[x] as f32)),
            &RED,
        ))
        .unwrap();
}

pub fn heat_chart(matrix: &Vec<Vec<f64>>, color_gradient: fn(f64) -> RGBColor, filename: &String) {
    let root = BitMapBackend::new(filename, (1024, 768)).into_drawing_area();
    root.fill(&WHITE).unwrap();

    let size = (matrix[0].len() as i32, matrix.len() as i32);

    let mut chart = ChartBuilder::on(&root)
        .margin(20u32)
        .top_x_label_area_size(50u32)
        .y_label_area_size(50u32)
        .build_cartesian_2d(0i32..size.0, size.1..0i32)
        .unwrap();

    chart
        .configure_mesh()
        .x_labels(20)
        .y_labels(20)
        .y_desc("state")
        .x_desc("state")
        .label_style(("sans-serif", 20))
        .draw()
        .unwrap();

    let serie = matrix
        .iter()
        .zip(0..)
        .map(|(l, y)| l.iter().zip(0..).map(move |(v, x)| (x as i32, y as i32, v)))
        .flatten()
        .map(|(x, y, v)| Rectangle::new([(x, y), (x + 1, y + 1)], color_gradient(*v).filled()));
    chart.draw_series(serie).unwrap();
}

// https://www.andrewnoske.com/wiki/Code_-_heatmaps_and_color_gradients
#[allow(dead_code)]
pub fn green_color_gradient(value: f64) -> RGBColor {
    let a_r = 204;
    let a_g = 255;
    let a_b = 204; // RGB for our 1st color (blue in this case).

    let b_r = 0;
    let b_g = 51;
    let b_b = 0; // RGB for our 2nd color (red in this case).

    let red = (b_r - a_r) as f64 * value + a_r as f64;
    let green = (b_g - a_g) as f64 * value + a_g as f64;
    let blue = (b_b - a_b) as f64 * value + a_b as f64;

    RGBColor(red as u8, green as u8, blue as u8)
}

#[allow(dead_code)]
pub fn grey_color_gradient(value: f64) -> RGBColor {
    RGBColor(
        (value * 255.0) as u8,
        (value * 255.0) as u8,
        (value * 255.0) as u8,
    )
}
#[allow(dead_code)]
pub fn red_white_color_gradient(value: f64) -> RGBColor {
    let a_r = 0;
    let a_g = 0;
    let a_b = 0; // RGB for our 1st color (blue in this case).

    let b_r = 255;
    let b_g = 0;
    let b_b = 0; // RGB for our 2nd color (red in this case).

    let red = (b_r - a_r) as f64 * value + a_r as f64;
    let green = (b_g - a_g) as f64 * value + a_g as f64;
    let blue = (b_b - a_b) as f64 * value + a_b as f64;

    RGBColor(red as u8, green as u8, blue as u8)
}
