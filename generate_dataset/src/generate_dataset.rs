use operator_advisor::model::*;
use operator_advisor::observation::ObservationType;
use serde_json;
use std::collections::HashMap;
use std::fs;
use std::path::PathBuf;

pub(crate) fn generate_datasets() {
    println!("Starting to generate data");
    println!("Generate observation files");
    generate_observation_file();
    println!("Observation files was generated successfully");
    println!("Generate training files");
    generate_training_data_file();
    println!("observation files was generated successfully");
    println!("Generate observation training files");
    generate_observation_training_data();
    println!("observation training files was generated successfully");
    println!("State occurences file");
    generate_occurence_dataset();
    println!("State occurences file was generated successfully");
    println!("State transition file");
    generate_transition_dataset();
    generate_transition_ignore_same_dataset();
    println!("State transition file was generated successfully");    
}

fn generate_observation_file() {
    let paths = PathBuf::from("../data/");
    generate_observation_file_generic(
        ObservationType::ContinuousObservation,
        &paths,
        &PathBuf::from("../dataset/observation.json"),
    );

    generate_observation_file_generic(
        ObservationType::ContinuousObservationClosestTree,
        &paths,
        &PathBuf::from("../dataset/observation_closest_tree.json"),
    );

    generate_observation_file_generic(
        ObservationType::ContinuousObservationNoTree,
        &paths,
        &PathBuf::from("../dataset/observation_no_tree.json"),
    );

    generate_observation_file_generic(
        ObservationType::ContinuousObservationSimpleMachineInformation,
        &paths,
        &PathBuf::from("../dataset/observation_simple_machine.json"),
    );
}

fn generate_observation_file_generic(
    obs_type: ObservationType,
    data_path: &PathBuf,
    output_path: &PathBuf,
) {
    create_observation_file(data_path.clone(), 1, output_path.clone(), &obs_type)
        .expect("was not able to create the for observation file");
}

fn generate_training_data_file() {
    let paths = PathBuf::from("../data/");
    generate_training_data_file_generic(
        ObservationType::ContinuousObservation,
        &paths,
        &PathBuf::from("../dataset/training_data_observation.json"),
    );
    generate_training_data_file_generic(
        ObservationType::ContinuousObservationClosestTree,
        &paths,
        &PathBuf::from("../dataset/training_data_observation_closest_tree.json"),
    );

    generate_training_data_file_generic(
        ObservationType::ContinuousObservationNoTree,
        &paths,
        &PathBuf::from("../dataset/training_data_observation_no_tree.json"),
    );

    generate_training_data_file_generic(
        ObservationType::ContinuousObservationSimpleMachineInformation,
        &paths,
        &PathBuf::from("../dataset/training_data_observation_simple_machine.json"),
    );
}

fn generate_training_data_file_generic(
    obs_type: ObservationType,
    data_path: &PathBuf,
    output_path: &PathBuf,
) {
    create_training_data_continuous_observation_file(
        data_path.clone(),
        1,
        output_path.clone(),
        &obs_type,
    )
    .expect("was not able to create the training file");
}

fn generate_observation_training_data() {
    let paths = PathBuf::from("../data/");
    generate_observation_training_data_generic(
        ObservationType::ContinuousObservation,
        &paths,
        &PathBuf::from("../dataset/training_data_observation_state.json"),
    );

    generate_observation_training_data_generic(
        ObservationType::ContinuousObservationClosestTree,
        &paths,
        &PathBuf::from("../dataset/training_data_observation_state_closest_tree.json"),
    );

    generate_observation_training_data_generic(
        ObservationType::ContinuousObservationNoTree,
        &paths,
        &PathBuf::from("../dataset/training_data_observation_state_no_tree.json"),
    );

    generate_observation_training_data_generic(
        ObservationType::ContinuousObservationSimpleMachineInformation,
        &paths,
        &PathBuf::from("../dataset/training_data_observation_state_simple_machine.json"),
    );

    
}

fn generate_observation_training_data_generic(
    obs_type: ObservationType,
    data_path: &PathBuf,
    output_path: &PathBuf,
) {
    create_training_observation_state_file(data_path.clone(), output_path.clone(), 1, obs_type)
        .expect("was not able to create the training file");
}

fn occurence(data: &Vec<usize>) -> HashMap<usize, usize> {
    let occurence_map = {
        let mut resp: HashMap<usize, usize> = HashMap::new();
        for el in data {
            if let Some(v) = resp.get_mut(el) {
                *v += 1;
            } else {
                resp.insert(*el, 1);
            }
        }
        resp
    };
    occurence_map
}

fn generate_occurence_dataset() {
    let data_path = PathBuf::from("../data/");
    let output_path = PathBuf::from("../dataset/occurences.json");
    let obs_type = ObservationType::ContinuousObservation;
    let training_data = get_training_observation_state(data_path.clone(), 1, obs_type)
        .expect("was not able to get the data");
    println!("number of training data {}", training_data.data.len());
    let n_observation: usize = training_data.data.values().map(|x| x.len()).sum();
    println!("number of labeled observation {}", n_observation);
    let average_test_run: f32 = n_observation as f32 / training_data.data.len() as f32;
    println!("average lenght of test run {}", average_test_run);
    let states: Vec<usize> = training_data
        .data
        .values()
        .flatten()
        .map(|x| x.state)
        .collect();
    let occ = occurence(&states);
    let json = serde_json::json!(occ);
    let j = serde_json::to_string(&json).expect("unable to create a json string");
    fs::write(output_path, j).expect("Unable to write file");
}

fn transition_probability(training_data:TrainingDataObservationState, n_state:usize)->Vec<Vec<f64>>{
    let states:Vec<usize> = training_data.data
            .values()
            .flatten()
            .map(|x| x.state).collect();
    let mut state_transition: Vec<Vec<f64>> = vec![vec![0.0;n_state]; n_state];
    for i in 0..states.len()-1 {
        state_transition[states[i]][states[i+1]]+=1.0
    }
    
    for states in state_transition.iter_mut(){
        let sum:f64 = states.iter().sum();
        if sum!=0.0{
            for el in states.iter_mut(){
            *el/=sum;
            }
        }
    }
    state_transition
}

fn transition_probability_ignore_same(training_data:TrainingDataObservationState, n_state:usize)->Vec<Vec<f64>>{
    let states:Vec<usize> = training_data.data
            .values()
            .flatten()
            .map(|x| x.state).collect();
    let mut state_transition: Vec<Vec<f64>> = vec![vec![0.0;n_state]; n_state];
    
    for i in 0..states.len()-1 {
        if states[i] !=states[i+1]{
            state_transition[states[i]][states[i+1]]+=1.0
        }
        
    }
    
    for states in state_transition.iter_mut(){
        let sum:f64 = states.iter().sum();
        if sum!=0.0{
            for el in states.iter_mut(){
            *el/=sum;
            }
        }
    }
    state_transition
}

fn generate_transition_dataset() {
    let n_state = 18;
    let data_path = PathBuf::from("../data/");
    let output_path = PathBuf::from("../dataset/transition.json");
    let obs_type = ObservationType::ContinuousObservation;
    let training_data = get_training_observation_state(data_path, 1, obs_type).expect("was not able to get the data");
    let state_transition = transition_probability(training_data,n_state);
    
    let json = serde_json::json!({"data": state_transition});
    let j = serde_json::to_string(&json).expect("unable to create a json string");
    fs::write(output_path, j).expect("Unable to write file");
}

fn generate_transition_ignore_same_dataset() {
    let n_state = 18;
    let data_path = PathBuf::from("../data/");
    let output_path = PathBuf::from("../dataset/transition_ignore_same.json");
    let obs_type = ObservationType::ContinuousObservation;
    let training_data = get_training_observation_state(data_path, 1, obs_type).expect("was not able to get the data");
    let state_transition = transition_probability_ignore_same(training_data,n_state);
    
    let json = serde_json::json!({"data": state_transition});
    let j = serde_json::to_string(&json).expect("unable to create a json string");
    fs::write(output_path, j).expect("Unable to write file");
}