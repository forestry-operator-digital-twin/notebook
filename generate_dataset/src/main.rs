mod generate_dataset;
mod generate_plot;
mod plot_helper;

fn main() {
    let separator = "======================================================================";
    println!("The datasets and the graph associated with the dataset will be produced");
    println!("{}", separator);
    println!("Datasets generation");
    println!("{}", separator);
    generate_dataset::generate_datasets();
    println!("{}", separator);
    println!("Graph of the datasets generation");
    generate_plot::generate_plot();
    println!("{}", separator);
    println!("================================^-Done Have a Revolutionnary Day-^================================");
}
