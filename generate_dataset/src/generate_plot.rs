use crate::plot_helper::*;

use operator_advisor::model::*;
use operator_advisor::observation::util::{
    convert_observations_to_feature_vector, get_feature_from_observation, ContinuousFeature,
    Feature,
};
use operator_advisor::observation::ObservationType;
use std::path::PathBuf;
use strum::IntoEnumIterator;

pub(crate) fn generate_plot() {
    let paths = PathBuf::from("../data/");
    let observation_type = ObservationType::ContinuousObservation;
    let data = get_all_observation_from_data_path(paths.clone(), 1, &observation_type).unwrap();
    let continuous_data = convert_observations_to_feature_vector(&data);
    let dest_folder = String::from("../figure");

    println!("Starting to generate graph");
    println!("Generate continuous dataset graphs");
    generate_continuous_observation(&continuous_data, &dest_folder);
    println!("Continuous dataset graphs was generated successfully");

    println!("Generate state transition graph");
    generate_state_transition(&paths, &dest_folder);
    generate_state_transition_ignore_same(&paths, &dest_folder);
    println!("State transition graph was generated successfully");
}

fn generate_continuous_observation(continuous_data: &Vec<Vec<f32>>, dest_folder: &String) {
    for f in ContinuousFeature::iter() {
        let feature = Feature::ContinuousFeature(f);

        let feature_vector: Vec<f32> = get_feature_from_observation(&continuous_data, &feature);

        let higest_value: f32 = feature_vector.clone().into_iter().reduce(f32::max).unwrap();
        serie_float(
            &feature_vector,
            higest_value,
            &format!("{}/{}_observation.png", dest_folder, feature),
        );
    }
}
#[allow(dead_code)]
fn generate_frequency_of_state(paths: &PathBuf, dest_folder: &String) {
    let observation_type = ObservationType::ContinuousObservation;
    let training_data = get_training_observation_state(paths.to_path_buf(), 1, observation_type)
        .expect("was not able to create the json");
    let states: Vec<usize> = training_data
        .data
        .values()
        .flatten()
        .map(|x| x.state)
        .collect();
    let max_occurrence = 1600;
    let n_state = 17;
    histogram(
        &states,
        n_state,
        max_occurrence,
        "state".to_string(),
        10,
        &format!("{}/state_frequency.png", dest_folder),
    )
}

fn generate_state_transition(paths: &PathBuf, dest_folder: &String) {
    let n_state = 18;
    let observation_type = ObservationType::ContinuousObservation;
    let training_data = get_training_observation_state(paths.clone(), 1, observation_type)
        .expect("was not able to get the data");
    let state_transition = transition_probability(training_data, n_state);
    heat_chart(
        &state_transition,
        grey_color_gradient,
        &format!("{}/state_transition.png", dest_folder),
    );
}

fn generate_state_transition_ignore_same(paths: &PathBuf, dest_folder: &String) {
    let n_state = 18;
    let observation_type = ObservationType::ContinuousObservation;
    let training_data = get_training_observation_state(paths.clone(), 1, observation_type)
        .expect("was not able to get the data");
    let state_transition = transition_probability_ignore_same(training_data, n_state);
    heat_chart(
        &state_transition,
        grey_color_gradient,
        &format!("{}/state_transition_ignore_same.png", dest_folder),
    );
}

fn transition_probability(
    training_data: TrainingDataObservationState,
    n_state: usize,
) -> Vec<Vec<f64>> {
    let states: Vec<usize> = training_data
        .data
        .values()
        .flatten()
        .map(|x| x.state)
        .collect();
    let mut state_transition: Vec<Vec<f64>> = vec![vec![0.0; n_state]; n_state];

    for i in 0..states.len() - 1 {
        state_transition[states[i]][states[i + 1]] += 1.0
    }

    for states in state_transition.iter_mut() {
        let sum: f64 = states.iter().sum();
        if sum != 0.0 {
            for el in states.iter_mut() {
                *el /= sum;
            }
        }
    }
    state_transition
}

fn transition_probability_ignore_same(training_data:TrainingDataObservationState, n_state:usize)->Vec<Vec<f64>>{
    let states:Vec<usize> = training_data.data
            .values()
            .flatten()
            .map(|x| x.state).collect();
    let mut state_transition: Vec<Vec<f64>> = vec![vec![0.0;n_state]; n_state];
    
    for i in 0..states.len()-1 {
        if states[i] !=states[i+1]{
            state_transition[states[i]][states[i+1]]+=1.0
        }
        
    }
    
    for states in state_transition.iter_mut(){
        let sum:f64 = states.iter().sum();
        if sum!=0.0{
            for el in states.iter_mut(){
            *el/=sum;
            }
        }
    }
    state_transition
}
