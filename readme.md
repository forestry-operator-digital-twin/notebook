# AI Model

Model to represent and predict the actions of a forest operator using [PyTorch](https://pytorch.org/). The hyper prameters are optimised using [Ray](https://docs.ray.io/en/latest/tune/index.html) and [Ax](https://ax.dev/).

## Dataset generation
`dataset_analysis.ipynb`, `generate_dataset` [rust](https://www.rust-lang.org/) 

This script transform the data from [a forestry operator simulation](https://gitlab.com/forestry-operator-digital-twin/data) into datasets using the [operator_advisor library](https://gitlab.com/forestry-operator-digital-twin/operator-advisor). Datasets are stored in `./dataset` for the AI models. The script also produces analytics information of the datasets.


## Models

## Dense Neural Network
`neural_network/NN.py`,
`nn.ipynb`

A simple dense neural network to transform the observations into actions of the operators.

## Recurrent Neural Network (RNN)
`neural_network/RNN.py`, 
`rnn.ipynb`

An RNN to predict the next observations or states of the operator

## Recurrent Neural Network with two head
`neural_network/RNN_NN_2Head.py`, 
`rnn_nn_2_head.ipynb`

An RNN with a prediction head and a classification head to predict the next observations and states of the operator
